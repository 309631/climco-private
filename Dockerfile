# base image
FROM node

# install dependencies 
RUN apt-get update
RUN apt-get -y install python3 python3-pip

COPY . /app/
WORKDIR /app/
RUN pip install -r requirements.txt

# compile frontend
WORKDIR /app/front/
RUN yarn install
RUN yarn build
WORKDIR /app/

# run python                TODO: Make config a volume
ENV FLASK_APP=climco
EXPOSE 5000
CMD ["flask", "run", "--host", "0.0.0.0"]

