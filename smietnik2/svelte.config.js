import adapter from '@sveltejs/adapter-auto';



// const browser: true;

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
	  adapter: adapter(),



		// Override http methods in the Todo forms
		methodOverride: {
			allowed: ['PATCH', 'DELETE']
		},
	     vite: {
     resolve: {
       dedupe: ['@fullcalendar/common'],
       browser: true,
     },
     optimizeDeps: {
       include:['@fullcalendar/common']
     }
   }
	  
	}
	};

export default config;
