

# Welcome to Climco - Supla's app to analyse and control 

App was created to analyse greenhouse parameters, as offficial app does not meet these requirements. 

# What is meant to be in this app:

- Chart with humidity and temperature (For now chartscss, or just generated from matplotlib/ in the future interactive JS chart)
- table with average/min/max/amplitude of humidity and temperature
- recommended temperature/humidity values based on plants
- integration with PlantCV
- the journal

# What is working right now:

 - The chart (more or less)
 - weather stats API for the server
 - There is copy-pasted Python PlantCV script
 - There is a calendar, but its ugly, and not functional at all
 

# How to install prealpha version:

Be aware that's even prealpha version, and even one feature is not working properly

1. Clone the repo:

```bash
git clone https://gitlab.com/309631/climco-private.git
```


2.  Into terminal create virtual environment by writing

```bash
python -m virtualenv env
```

If python command is not found try ```python3  -m virtualenv env```

7.  activate virtual enviroment;

```bash
source venv/bin/activate
```


2. Install the requirements by writing in the console:

```bash
pip install -r requrements.txt
```
2. Open supla.org, login into your account, and go to Account > Integrations > My OAuth apps > Register new OAuth application

3. Into the name and description tabs you can write whatever you want

4. Into authorization callback URLs put: http://127.0.0.1:5000/auth

5. Click  Register a new OAuth application buttons

6. Create config.py in climco folder and put info from the Configuration tab in Supla OAuth App:

```python
SUPLA_CLIENT_ID = 'YOUR PUBLIC ID FROM OAUTH APPLICATION TAB'
SUPLA_CLIENT_SECRET = 'YOUR SECRET FROM OAUTH APP APPLICATION TAB'
```


7. Go into front folder and write:

```bash
yarn install
yarn build
```

7. After all open the terminal in your installation location and write: 

```bash
export FLASK_APP=climco
flask run
```

There is also a makefile, so if you have make, you should be able to run the app, by writing 
`make`
in the terminal

The app was tested only under Debian GNU/Linux 11 (bullseye) and right now under Debian GNU/Linux 12 (bookworm) on Firefox, Qutebrowser and Brave-Browser (on any of them is working correctly, but its working on each of them)
