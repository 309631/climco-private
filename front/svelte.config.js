import adapter from '@sveltejs/adapter-static';
import { isoImport } from 'vite-plugin-iso-import'

const dev = process.env.NODE_ENV === 'development';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit:
  {
    adapter: adapter({
      // default options are shown
      pages: 'build',
      assets: 'build',
      fallback: null,
      precompress: false
    }),
    trailingSlash: 'always',
    paths: {
      base: dev ? '' : '/app',
    },
    prerender: {
      // This can be false if you're using a fallback (i.e. SPA mode)
      default: true
    },
    vite: {
      plugins: [
        isoImport(),
      ],
      resolve: {
      dedupe: ['@fullcalendar/common'],
      browser: true,
     },
     optimizeDeps: {
       include:['@fullcalendar/common']
     }
   }
  },
  paths: {
    base: 'app/'
  },
  ssr:false,
};

export default config;
